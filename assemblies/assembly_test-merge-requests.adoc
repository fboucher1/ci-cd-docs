ifdef::context[:parent-context-of-assembly_test-merge-requests: {context}]

ifndef::context[]
[id="assembly_test-merge-requests"]
endif::[]
ifdef::context[]
[id="assembly_test-merge-requests_{context}"]
endif::[]

:context: assembly_test-merge-requests

// [role="_abstract"]
// TBA

= Overview

include::../modules/con_zuul-and-nodepool.adoc[leveloffset=+1]
include::../modules/ref_configuration-repositories.adoc[leveloffset=+1]

= How to

include::../modules/proc_how-to-configure-node-type.adoc[leveloffset=+1]
include::../modules/proc_how-to-enable-dist-git-tests-on-merge-request.adoc[leveloffset=+1]

// [role="_additional-resources"]
// == Additional resources (or Next steps)
// * TBA

ifdef::parent-context-of-assembly_test-merge-requests[:context: {parent-context-of-assembly_test-merge-requests}]
ifndef::parent-context-of-assembly_test-merge-requests[:!context:]

